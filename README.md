# vcmp-dotnet

vcmp-dotnet is a collection of softwares that allow you to script [Vice City: Multiplayer](https://vc-mp.org) servers in any language that supports targeting CoreCLR (C#, F# and VB.Net for example).

> This readme file is currently oriented towards the native API binding and may be updated when the wrapper module is released.

## Compiling

### Host plugin

> The instructions here are tested on Linux. They may or may not work on Windows. Refer to Meson's documentation to compile this on Windows.

The host plugin uses the [Meson build system](http://mesonbuild.com/) for compilation. To compile, initialize a build directory and run [ninja](https://ninja-build.org/) in it. Subsequent compilations do not require recreating a build directory.

```bash
meson build
ninja -c build
```

Your plugin will be available as `libdotnet-vcmp.so` in your build directory.

### Native API binding

The native API binding does not have any special build instructions. It uses [MSBuild](https://msdn.microsoft.com/en-us/library/dd393574.aspx) for compilation.

```bash
dotnet build
```

## Usage

### dotnet.json

You need to have a file called `dotnet.json` in your server's root.

This is a list of keys the file must contain:

| Key                                      | Description                                        |
| ---------------------------------------- | -------------------------------------------------- |
| `clr.runtime-directory`                  | The directory that contains libcoreclr.so/dll.     |
| `clr.server-executable`                  | The path to the server binary.                     |
| `clr.assemblies-directory`               | The path to your .NET managed assemblies.          |
| `clr.native-image-assemblies-directory`  | Same as the above but for native image assemblies. |
| `clr.native-dll-search-directories`      | The directory to search for dynamic libraries.     |
| `app-domain.friendly-name`               | The friendly name of the default AppDomain.        |

Here is an example `dotnet.json` file:

```json
{
    "clr": {
        "runtime-directory": "/usr/share/dotnet/shared/Microsoft.NETCore.App/2.1.2",
        "server-executable": "./mpsvrrel64",
        "assemblies-directory": "./dotnet-assemblies",
        "native-image-assemblies-directory": "",
        "native-dll-search-directories": ["/usr/lib"]
    },
    "app-domain": {
        "friendly-name": "Example dotnet-vcmp configuration"
    }
}
```

### Interoperability DLL

You need to place `Fleka.DotnetVcmp.Interop.dll` in your assemblies directory. Add `Fleka.DotnetVcmp.Interop` as a NuGet dependency to your project.

### From managed code

The interoperability DLL executes an entry point with the following signature:

```cs
void EntryPointDelegate(PluginFunctions pluginFunctions, ref PluginCallbacks pluginCallbacks)
```

To designate a function as the entry point, add `[Fleka.DotnetVcmp.Interop.EntryPoint]` before its definition.

> **IMPORTANT**

> Assigning a function to `pluginCallbacks` will _not_ increase its reference count.
> If you are passing a collectable function (such as a lambda), you _must_ keep a
> reference to it elsewhere as well.

### Hello world

Compile this C# code into a .NET managed DLL (with `Fleka.DotnetVcmp.Interop` as a dependency) and place both of them in your assemblies directory.

```cs
public static class MyTestApp {
    private static Fleka.DotnetVcmp.Interop.PluginFunctions _pluginFunctions;

    [Fleka.DotnetVcmp.Interop.EntryPoint]
    public static void MyEntryPoint(Fleka.DotnetVcmp.Interop.PluginFunctions pluginFunctions, ref Fleka.DotnetVcmp.Interop.PluginCallbacks pluginCallbacks) {
        _pluginFunctions = pluginFunctions;

        pluginCallbacks.OnServerInitialize = OnServerInitialize;
    }

    internal static byte OnServerInitialize() {
        _pluginFunctions.LogMessage("%s", "Hello world");

        return 1;
    }
}
```

## Downloads

> You should not have to download the C# native bindings interoperability DLL manually
> under normal circumstances.
> Add it as a NuGet dependency instead.

- [VCMP native plugin (Linux x64)](https://gitlab.com/fleka_e/dotnet-vcmp/blob/master/release/libdotnet-vcmp-linux64.so)
- [C# native bindings interoperability DLL (x64)](https://gitlab.com/fleka_e/dotnet-vcmp/blob/master/release/Fleka.DotnetVcmp.Interop.dll)
