1.0.0:
- Everything.

1.1.0:
- The interoperability DLL now loads other assemblies in its directory so that the EntryPoint can be found. This fixes [issue #1](https://gitlab.com/fleka_e/dotnet-vcmp/issues/1).
